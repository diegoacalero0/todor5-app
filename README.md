# TODO R5

Flutter project TODO APP R5

## Description

This app provide a easy way to register and manage
tasks

### Features

- Login with email and password
- Register with email and password
- Recover password with a firebase email
- Add task
- Remove task
- Update task state
- Logout
- See task translate to English
