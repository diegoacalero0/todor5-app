import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_r5/data/data_sources/auth_data_souce.dart';

import 'auth_data_source_test.mocks.dart';

@GenerateMocks([FirebaseAuth, User, UserCredential])
void main() {

  final MockFirebaseAuth _mockFirebaseAuth = MockFirebaseAuth();

  late AuthDataSource _authDataSource;

  setUp(() {
    _authDataSource = AuthDataSource(firebaseAuth: _mockFirebaseAuth);
  });

  test("getCurrentUser Test", () {
    final MockUser user = MockUser();
    when(_mockFirebaseAuth.currentUser).thenReturn(user);
    final User? currentUser = _authDataSource.getCurrentUser();
    expect(user, currentUser);
    verify(_mockFirebaseAuth.currentUser);
  });

  test("registerWithEmailAndPassword Test", () async {
    final MockUser user = MockUser();
    final MockUserCredential userCredential = MockUserCredential();

    when(_mockFirebaseAuth.createUserWithEmailAndPassword(email: "email", password: "password")).thenAnswer((realInvocation) async { return userCredential; });
    when(_mockFirebaseAuth.currentUser).thenReturn(user);

    final User? currentUser = await _authDataSource.registerWithEmailAndPassword("email", "password");
    

    expect(user, currentUser);
    verify(_mockFirebaseAuth.createUserWithEmailAndPassword(email: "email", password: "password"));
    verify(_mockFirebaseAuth.currentUser);
  });

  test("logout Test", () {
    _authDataSource.logout();
    verify(_mockFirebaseAuth.signOut());
  });

  test("loginWithEmailAndPassword Test", () async {
    final MockUser user = MockUser();
    final MockUserCredential userCredential = MockUserCredential();

    when(_mockFirebaseAuth.signInWithEmailAndPassword(email: "email", password: "password")).thenAnswer((realInvocation) async { return userCredential; });
    when(_mockFirebaseAuth.currentUser).thenReturn(user);

    final User? currentUser = await _authDataSource.loginWithEmailAndPassword("email", "password");
    
    expect(user, currentUser);
    verify(_mockFirebaseAuth.signInWithEmailAndPassword(email: "email", password: "password"));
    verify(_mockFirebaseAuth.currentUser);
  });

  test("sendRecoverPasswordEmail Test", () async {
    _authDataSource.sendRecoverPasswordEmail("email");
    verify(_mockFirebaseAuth.sendPasswordResetEmail(email: "email"));
  });

}