import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_r5/constants/firebase_constants.dart';
import 'package:todo_r5/data/data_sources/tasks_data_source.dart';
import 'task_data_source_test.mocks.dart';

@GenerateMocks([FirebaseAuth, User, FirebaseFirestore, CollectionReference, DocumentReference])
void main() {

  final MockCollectionReference<Map<String, dynamic>> _mockCollectionUsers = MockCollectionReference();
  final MockCollectionReference<Map<String, dynamic>> _mockCollectionTasks = MockCollectionReference();

  final MockDocumentReference<Map<String, dynamic>> _mockDocumentUser = MockDocumentReference();
  final MockDocumentReference<Map<String, dynamic>> _mockDocumentTask = MockDocumentReference();

  final MockFirebaseAuth _mockFirebaseAuth = MockFirebaseAuth();
  final MockFirebaseFirestore _mockFirebaseFirestore = MockFirebaseFirestore();
  final MockUser _mockUser = MockUser();


  late TasksDataSource _taskDataSource;

  setUp(() {
    _taskDataSource = TasksDataSource(firebaseAuth: _mockFirebaseAuth, firestore: _mockFirebaseFirestore);
    when(_mockFirebaseAuth.currentUser).thenReturn(_mockUser);
    when(_mockUser.email).thenReturn("diegoacalero0@gmail.com");
    when(_mockFirebaseFirestore.collection(kFirestoreUsersCollection)).thenReturn(_mockCollectionUsers);
    when(_mockCollectionUsers.doc("diegoacalero0@gmail.com")).thenReturn(_mockDocumentUser);
    when(_mockDocumentUser.collection(kFirestoreTasksCollection)).thenReturn(_mockCollectionTasks);
    when(_mockCollectionTasks.doc(any)).thenReturn(_mockDocumentTask);
    
    

    when(_mockCollectionTasks.add(any)).thenAnswer((realInvocation) async { return _mockDocumentTask; });
    
  });

  test("createTask Test", () {
    _taskDataSource.createTask("Hola", "Hola");
    verify(_mockFirebaseFirestore.collection(kFirestoreUsersCollection));
    verify(_mockCollectionUsers.doc("diegoacalero0@gmail.com"));
    verify(_mockDocumentUser.collection(kFirestoreTasksCollection));
    verify(_mockCollectionTasks.add(any));
  });

}