import 'package:flutter/material.dart';
import 'package:todo_r5/constants/colors.dart';

class CustomFilledTextFormField extends StatelessWidget {

  final String hintText;
  final bool? obscureText;
  final TextInputType? keyboardType;
  final String? prefixIcon;
  final String? Function(String?)? validator;
  final TextEditingController? controller;
  final Function(String)? onChanged;

  int? maxLines;
  Color borderColor;
  double borderWith;
  TextInputAction? textInputAction; 

  CustomFilledTextFormField({
    required this.hintText,
    this.controller,
    this.obscureText,
    this.keyboardType,
    this.prefixIcon,
    this.validator,
    this.borderColor = kFieldsBackgroundColorLight,
    this.borderWith = 0,
    this.maxLines = 1,
    this.textInputAction,
    this.onChanged,
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      textInputAction: textInputAction,
      maxLines: maxLines,
      controller: controller,
      validator: validator,
      keyboardType: keyboardType,
      obscureText: obscureText ?? false,
      style: Theme.of(context).textTheme.bodyText1?.copyWith(
        fontSize: 16
      ),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
        
        prefixIconConstraints: const BoxConstraints(
          
        ),
        prefixIcon: prefixIcon != null ? 
          Container(
            padding: const EdgeInsets.only(left: 20, right: 16),
            child: Image.asset(prefixIcon ?? "", color: Colors.white.withOpacity(0.4),),
          )
        : null,
        filled: true,
        fillColor: kFieldsBackgroundColorLight,
        hintText: hintText,
        hintStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
          fontSize: 16,
          color: Colors.black.withOpacity(0.5)
        ),
        errorMaxLines: 2,
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(color: Color(0xffD2416F), width: 1)
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: borderColor, width: borderWith)
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: borderColor, width: borderWith)
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: borderColor, width: borderWith)
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(color: Color(0xffD2416F), width: 1.5)
        ),
        errorStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
          color: const Color(0xffE42F6F),
          fontSize: 11,
        )
      ),
    );
  }

}