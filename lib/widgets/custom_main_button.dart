import 'package:flutter/material.dart';
import 'package:todo_r5/constants/colors.dart';

class CustomMainButton extends StatelessWidget {

  final Color backgroundColor;
  final Color textColor;
  final String text;
  final VoidCallback onTap;
  final TextStyle? textStyle;

  const CustomMainButton({
    required this.text,
    required this.onTap,
    this.textStyle,
    this.backgroundColor = kPrimaryColor,
    this.textColor = Colors.white,
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onTap,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.pressed)) {
            return backgroundColor.withOpacity(0.7);
          }
          return backgroundColor;
        }),
        shape: MaterialStateProperty.resolveWith((states) {
          return RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)
          );
        }),
      ),
      child: SizedBox(
        width: double.infinity,
        height: 48,
        child: Center(
          child: Text(
            text,
            style: textStyle ?? Theme.of(context).textTheme.headline1?.copyWith(
              color: textColor,
              fontSize: 16
            ),
          ),
        )
      )
    );
  }

}