import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/route_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/constants/colors.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:todo_r5/screens/create_task_screen.dart';
import 'package:todo_r5/screens/login_screen.dart';
import 'package:todo_r5/screens/recover_password_screen.dart';
import 'package:todo_r5/screens/register_screen.dart';
import 'package:todo_r5/screens/todo_list_screen.dart';
import 'firebase_options.dart';
import 'package:todo_r5/screens/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  setUpLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: EasyLoading.init(),
      getPages: [
        GetPage(name: "/", page: () => const SplashScreen(key: Key("SplashScreen"))),
        GetPage(name: "/login", page: () => const LoginScreen(key: Key("LoginScreen"))),
        GetPage(name: "/register", page: () => const RegisterScreen(key: Key("Register"))),
        GetPage(name: "/recoverPassword", page: () => const RecoverPasswordScreen(key: Key("RecoverPasswordScreen"))),
        GetPage(name: "/todoList", page: () => const TodoListScreen(key: Key("TodoListScreen"))),
        GetPage(name: "/createTask", page: () => const CreateTaskScreen(key: Key("CreateTaskScreen")))
      ],
      home: const SplashScreen(key: Key("SplashScreen")),
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          titleTextStyle: TextStyle(
            fontSize: 20,
            color: Colors.white,
            fontFamily: "Neon"
          )
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.resolveWith((states) {
              return kPrimaryColor;
            }),
            textStyle: MaterialStateProperty.resolveWith((states) {
              return const TextStyle(
                decoration: TextDecoration.underline,
                fontFamily: "Neon"
              );
            }),
          )
        ),
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        primaryColor: kPrimaryColor,
        colorScheme: const ColorScheme.light(
          primary: kPrimaryColor,
          secondary: kAccentColor,
        ),
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontFamily: "Neon",
            fontSize: 32,
            color: Colors.black
          ),
          headline2: TextStyle(
            fontFamily: "Epilogue",
            fontSize: 24,
            color: Colors.black
          ),
          headline3: TextStyle(
            fontFamily: "Epilogue",
            fontSize: 24,
            fontStyle: FontStyle.italic,
            color: Colors.black
          ),
          headline4: TextStyle(
            fontFamily: "OpenSans",
            fontSize: 20,
            color: Colors.black
          ),
          headline5: TextStyle(
            fontFamily: "OpenSans",
            fontSize: 20,
            fontStyle: FontStyle.italic,
            color: Colors.black
          ),
          bodyText1: TextStyle(
            fontFamily: "Neon",
            fontSize: 16,
            color: Colors.black
          ),
          bodyText2: TextStyle(
            fontFamily: "Neon",
            fontSize: 16,
            color: Colors.black
          )
        ),
      ),
    );
  }
}
