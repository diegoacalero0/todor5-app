/// Collections
const kFirestoreUsersCollection = "users";
const kFirestoreTasksCollection = "tasks";

/// Fields
const kFirestoreTitleField = "title";
const kFirestoreDescriptionField = "description";
const kFirestoreCreatedAtField = "createdAt";
const kFirestoreIsCompletedField = "isCompleted";
const kFirestoreTranslatedTitleField = "translatedTitle";
const kFirestoreTranslatedDescriptionField = "translatedDescription";