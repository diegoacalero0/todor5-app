import 'package:flutter/material.dart';

const Color kPrimaryColor = Color(0xffA9B9E6);
const Color kAccentColor = Color(0xffBDADEA);
const Color kFieldsBackgroundColorLight = Color(0xffebeef0);