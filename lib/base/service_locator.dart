import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get_it/get_it.dart';
import 'package:todo_r5/data/data_sources/auth_data_souce.dart';
import 'package:todo_r5/data/data_sources/tasks_data_source.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';
import 'package:todo_r5/data/repositories/tasks_repository.dart';
import 'package:todo_r5/use_cases/auth/create_user_use_case.dart';
import 'package:todo_r5/use_cases/auth/get_current_user_use_case.dart';
import 'package:todo_r5/use_cases/auth/login_with_email_and_password_use_case.dart';
import 'package:todo_r5/use_cases/auth/logout_use_case.dart';
import 'package:todo_r5/use_cases/auth/send_recover_password_email_use_case.dart';
import 'package:todo_r5/use_cases/tasks/change_task_state_use_case.dart';
import 'package:todo_r5/use_cases/tasks/create_task_use_case.dart';
import 'package:todo_r5/use_cases/tasks/get_tasks_use_case.dart';
import 'package:todo_r5/use_cases/tasks/remove_task_use_case.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';

final locator = GetIt.instance;

void setUpLocator() {

  /// Utiltiies
  locator.registerLazySingleton<EasyLoading>(() => EasyLoading());
  locator.registerLazySingleton<AlertsUtils>(() => AlertsUtils());

  /// Firebase
  locator.registerLazySingleton<FirebaseAuth>(() => FirebaseAuth.instance);
  locator.registerLazySingleton<FirebaseFirestore>(() => FirebaseFirestore.instance);

  /// Data Sources
  locator.registerLazySingleton<AuthDataSource>(() => AuthDataSource());
  locator.registerLazySingleton<TasksDataSource>(() => TasksDataSource());

  /// Repositories
  locator.registerLazySingleton<AuthRepository>(() => AuthRepository());
  locator.registerLazySingleton<TasksRepository>(() => TasksRepository());

  /// Use Cases
  locator.registerLazySingleton<GetCurrentUserUseCase>(() => GetCurrentUserUseCase());
  locator.registerLazySingleton<CreateUserUseCase>(() => CreateUserUseCase());
  locator.registerLazySingleton<LogoutUseCase>(() => LogoutUseCase());
  locator.registerLazySingleton<LoginWithEmailAndPasswordUseCase>(() => LoginWithEmailAndPasswordUseCase());
  locator.registerLazySingleton<SendRecoverPasswordUseCase>(() => SendRecoverPasswordUseCase());
  locator.registerLazySingleton<CreateTaskUseCase>(() => CreateTaskUseCase());
  locator.registerLazySingleton<GetTasksUseCase>(() => GetTasksUseCase());
  locator.registerLazySingleton<RemoveTaskUseCase>(() => RemoveTaskUseCase());
  locator.registerLazySingleton<ChangeTaskStateUseCase>(() => ChangeTaskStateUseCase());
  
}