import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';

///Use case that logouts the current logged user
class LogoutUseCase {
  final AuthRepository _authRepository;

  LogoutUseCase({
    AuthRepository? authRepository
  }): _authRepository = authRepository ?? locator.get<AuthRepository>();

  Future<void> invoke() => _authRepository.logout();
}