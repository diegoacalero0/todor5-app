import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';

/// Use case that returns the current
/// logged user
/// otherwise returns null
class GetCurrentUserUseCase {
  final AuthRepository _authRepository;

  GetCurrentUserUseCase({
    AuthRepository? authRepository
  }): _authRepository = authRepository ?? locator.get<AuthRepository>();

  User? invoke() {
    return _authRepository.getCurrentUser();
  }
}