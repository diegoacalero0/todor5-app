import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';

class SendRecoverPasswordUseCase {
  final AuthRepository _authRepository;

  SendRecoverPasswordUseCase({
    AuthRepository? authRepository
  }): _authRepository = authRepository ?? locator.get<AuthRepository>();

  Future<void> invoke(String email) => _authRepository.sendRecoverPasswordEmail(email);

}