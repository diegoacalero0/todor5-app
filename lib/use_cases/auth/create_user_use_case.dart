import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';

/// Use case that creates a new account
/// using the email and password
class CreateUserUseCase {
  final AuthRepository _authRepository;

  CreateUserUseCase({
    AuthRepository? authRepository
  }): _authRepository = authRepository ?? locator.get<AuthRepository>();

  Future<User?> invoke(String email, String password) async {
    return _authRepository.registerWithEmailAndPassword(email, password);
  }
}