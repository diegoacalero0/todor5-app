import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/auth_repository.dart';

/// Use case that login the user
/// using email and password
class LoginWithEmailAndPasswordUseCase {
  final AuthRepository _authRepository;

  LoginWithEmailAndPasswordUseCase({
    AuthRepository? authRepository
  }): _authRepository = authRepository ?? locator.get<AuthRepository>();

  Future<User?> invoke(String email, String password) => _authRepository.loginWithEmailAndPassword(email, password);
}