import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/tasks_repository.dart';

class ChangeTaskStateUseCase {
  final TasksRepository _tasksRepository;

  ChangeTaskStateUseCase({
    TasksRepository? tasksRepository
  }): _tasksRepository = tasksRepository ?? locator.get<TasksRepository>();

  Future<void> invoke(String taskId, bool newState) => _tasksRepository.changeTaskState(taskId, newState);

}