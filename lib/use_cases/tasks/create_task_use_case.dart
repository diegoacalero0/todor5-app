import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/tasks_repository.dart';

/// Use case that creates a new tasks
class CreateTaskUseCase {
  final TasksRepository _tasksRepository;

  CreateTaskUseCase({
    TasksRepository? tasksRepository
  }): _tasksRepository = tasksRepository ?? locator.get<TasksRepository>();

  Future<void> invoke(String title, String description) => _tasksRepository.createTask(title, description);

}