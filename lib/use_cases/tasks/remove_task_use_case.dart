import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/tasks_repository.dart';

class RemoveTaskUseCase {
  final TasksRepository _tasksRepository;

  RemoveTaskUseCase({
    TasksRepository? tasksRepository
  }): _tasksRepository = tasksRepository ?? locator.get<TasksRepository>();

  Future<void> invoke(String taskId) => _tasksRepository.removeTask(taskId);
}