import 'dart:async';

import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/repositories/tasks_repository.dart';
import 'package:todo_r5/models/task_model.dart';

class GetTasksUseCase {
  final TasksRepository _tasksRepository;

  GetTasksUseCase({
    TasksRepository? tasksRepository
  }): _tasksRepository = tasksRepository ?? locator.get<TasksRepository>();

  StreamController<List<TaskModel>> invoke() => _tasksRepository.getTasks();
}