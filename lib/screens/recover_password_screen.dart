import 'package:flutter/material.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/route_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:todo_r5/constants/assets_constants.dart';
import 'package:todo_r5/screens/controllers/recover_password_controller.dart';
import 'package:todo_r5/widgets/custom_main_button.dart';
import 'package:todo_r5/widgets/custom_text_form_field.dart';

class RecoverPasswordScreen extends StatefulWidget {

    const RecoverPasswordScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _RecoverPasswordScreenState();

}

class _RecoverPasswordScreenState extends State<RecoverPasswordScreen> {
  
  final RecoverPasswordController _controller = Get.put(RecoverPasswordController());

  final _formKey = GlobalKey<FormState>(); 
  final TextEditingController _emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Recuperar contraseña")),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _createLottie(),
                _createTextFieldEmail(),
                const SizedBox(height: 8),
                _createMainButton(),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          )
        ),
      )
    );
  }

  Widget _createLottie() {
    return SizedBox(
      width: 128,
      child: Lottie.asset(kLottieSplash)
    );
  }

  Widget _createTextFieldEmail() {

    return CustomFilledTextFormField(
      controller: _emailController,
      hintText: "Correo electrónico",
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);

        if(!emailValid) {
          return "Has ingresado un correo electrónico incorrecto. Inténtalo de nuevo.";
        }
      },
    );
  }


  Widget _createMainButton() {
    return CustomMainButton(
      onTap: () {
        if(_formKey.currentState?.validate() == true) {
          _controller.sendRecoverPasswordEmail(_emailController.text);
        }
      },
      text: "Enviar correo de recuperación"
    );
  }

}
