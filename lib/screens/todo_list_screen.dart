import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:todo_r5/constants/assets_constants.dart';
import 'package:todo_r5/models/task_model.dart';
import 'package:todo_r5/screens/controllers/logout_controller.dart';
import 'package:todo_r5/screens/controllers/todo_controller.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';
import 'package:todo_r5/utilities/date_utils.dart';
import 'package:todo_r5/widgets/custom_main_button.dart';

class TodoListScreen extends StatefulWidget {

    const TodoListScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _TodoListScreenState();

}

class _TodoListScreenState extends State<TodoListScreen> {

  final TodosController _controller = Get.put(TodosController());
  final LogoutController _logoutController = Get.put(LogoutController());

  @override
  void initState() {
    _controller.getTasks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listado de tareas"),
        actions: [
          IconButton(
            onPressed: _logoutController.logout,
            icon: const Icon(Icons.logout)
          )
        ],
      ),
      body: RefreshIndicator(
        child: Container(
          child: _createBody(),
        ),
        onRefresh: () async {
          _controller.getTasks();
        },
      ),
      floatingActionButton: Obx(() {
        if(_controller.tasks.isEmpty) {
          return const SizedBox();
        } else {
          return FloatingActionButton(
            onPressed: _navigateToCreateTask,
            child: const Icon(Icons.add, color: Colors.white),
          );
        }
      })
    );
  }

  Widget _createBody() {
    return Obx(() {
      if(_controller.tasks.isEmpty) {
        return _createEmptyState();
      } else {
        return _createTasksList();
      }
    });
  }

  Widget _createTasksList() {
    return Obx(() {
      return ListView.builder(
        
        padding: const EdgeInsets.only(top: 16, bottom: 84),
        itemBuilder: (BuildContext context, int index) => _createTask(_controller.tasks[index], index),
        itemCount: _controller.tasks.length
      );
    });
  }

  Widget _createTask(TaskModel task, int index) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(12),
        boxShadow: <BoxShadow>[
           BoxShadow(
              offset: const Offset(0, 3),
              spreadRadius: 3,
              blurRadius: 8,
              color: Theme.of(context).dividerColor
          )
        ]
      ),
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Checkbox(
            fillColor: MaterialStateProperty.resolveWith((states) => Theme.of(context).colorScheme.secondary),
            value: task.isCompleted,
            onChanged: (bool? value) {
              _controller.changeTaskState(task.id, value ?? false);
            }
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(formatDate(task.createdDate)),
                Text(task.title, style: Theme.of(context).textTheme.bodyText1),
                Text(task.description, style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  color: Colors.black.withOpacity(0.5)
                )),
                _createTranslateButton(task, index),
                _createTranslaction(task)
              ],
            ),
          ),
          _createDeleteButton(task)
        ],
      ),
    );
  }

  Widget _createDeleteButton(TaskModel task) {
    return IconButton(
      onPressed: () {
        AlertsUtils().showGenericAlert(
          title: "Eliminar tarea",
          message: "¿Está seguro que desea eliminar esta tarea?",
          actions: [
            TextButton(
              onPressed: () async {
                if(Get.overlayContext != null) {
                  Navigator.of(Get.overlayContext!).pop();
                }
                _controller.removeTask(task.id);
              },
              child: const Text("Eliminar")
            ),
            TextButton(
              onPressed: Get.back,
              child: const Text("Cancelar")
            )
          ]
        );
      },
      icon: const Icon(Icons.delete_outline, color: Colors.red)
    );
  }

  Widget _createTranslaction(TaskModel task) {
    if(task.showTranslate) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(task.translatedTitle ?? "Traducción no disponible", style: Theme.of(context).textTheme.bodyText1),
          Text(task.translatedDescription ?? "", style: Theme.of(context).textTheme.bodyText1?.copyWith(
            color: Colors.black.withOpacity(0.5)
          )),
        ],
      );
    } else {
      return const SizedBox();
    }
  }

  Widget _createTranslateButton(TaskModel task, int index) {
    if(task.translatedDescription == null && task.translatedTitle == null) {
      return const SizedBox();
    } else {
      return TextButton(
        onPressed: () {
          _controller.setShowTranslate(!task.showTranslate, index);
        },
        child: Text(task.showTranslate ? "Ocultar traducción" : "Traducir")
      );        
    }
  }

  Widget _createEmptyState() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              child: Lottie.asset(kLottieSplash, width: 128),
            ),
            const Text("No tienes tareas creadas"),
            Text(
              "Agrega nuevas tareas para tener un mejor control de tus actividades diarias",
              style: Theme.of(context).textTheme.bodyText2?.copyWith(
                color: Colors.black.withOpacity(0.6)
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16),
            CustomMainButton(
              text: "Agregar nueva tarea",
              onTap: _navigateToCreateTask
            )
          ],
        ),
      )
    );
  }

  void _navigateToCreateTask() {
    Get.toNamed("/createTask");
  }

}
