import 'package:flutter/material.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/route_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:todo_r5/constants/assets_constants.dart';
import 'package:todo_r5/screens/controllers/register_controller.dart';
import 'package:todo_r5/widgets/custom_main_button.dart';
import 'package:todo_r5/widgets/custom_text_form_field.dart';

class RegisterScreen extends StatefulWidget {

    const RegisterScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _RegisterScreenState();

}

class _RegisterScreenState extends State<RegisterScreen> {

  final RegisterController _controller = Get.put(RegisterController());

  final _formKey = GlobalKey<FormState>(); 
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Regístrate")),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _createLottie(),
                _createTextFieldEmail(),
                const SizedBox(height: 8),
                _createTextFieldPassword(),
                const SizedBox(height: 8),
                _createTextFieldRePassword(),
                const SizedBox(height: 8),
                _createMainButton(),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          )
        ),
      )
    );
  }

  Widget _createLottie() {
    return SizedBox(
      width: 128,
      child: Lottie.asset(kLottieSplash)
    );
  }

  Widget _createTextFieldEmail() {
    return CustomFilledTextFormField(
      controller: _emailController,
      hintText: "Correo electrónico",
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);

        if(!emailValid) {
          return "Has ingresado un correo electrónico incorrecto. Inténtalo de nuevo.";
        }
      },
    );
  }

  Widget _createTextFieldPassword() {
    return CustomFilledTextFormField(
      controller: _passwordController,
      hintText: "Contraseña",
      obscureText: true,
      textInputAction: TextInputAction.next,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  Widget _createTextFieldRePassword() {
    return CustomFilledTextFormField(
      hintText: "Repetir contraseña",
      obscureText: true,
      textInputAction: TextInputAction.done,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        } else if(text != _passwordController.text) {
          return "Las contraseñas deben ser idénticas";
        }
        return null;
      },
    );
  }

  Widget _createMainButton() {
    return CustomMainButton(
      onTap: () {
        if(_formKey.currentState?.validate() == true) {
          _controller.createUser(_emailController.text, _passwordController.text);
        }
      },
      text: "Crear cuenta"
    );
  }
}
