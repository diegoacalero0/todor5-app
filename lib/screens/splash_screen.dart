import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:todo_r5/constants/assets_constants.dart';
import 'package:todo_r5/screens/controllers/splash_controller.dart';

class SplashScreen extends StatefulWidget {

    const SplashScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen> {

  final SplashController _controller = Get.put(SplashController());

  @override
  void initState() {
    _controller.checkUserIsLogged();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          child: Lottie.asset(kLottieSplash),
          padding: const EdgeInsets.symmetric(horizontal: 64),
        )
      ),
    );
  }
}
