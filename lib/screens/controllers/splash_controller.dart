import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/use_cases/auth/get_current_user_use_case.dart';

class SplashController extends GetxController {

  final GetCurrentUserUseCase _getCurrentUserUseCase;

  SplashController({
    GetCurrentUserUseCase? getCurrentUserUseCase
  }): _getCurrentUserUseCase = getCurrentUserUseCase ?? locator.get<GetCurrentUserUseCase>();

  /// Method that validates if user is
  /// logged or not
  /// if logged navigates to home
  /// otherwise navigates to login
  void checkUserIsLogged() {
    Future.delayed(Duration(seconds: 2), () {
      final User? currentUser = _getCurrentUserUseCase.invoke();
      if(currentUser == null) {
        Get.offAndToNamed("/login");
      } else {
        Get.offAndToNamed("/todoList");
      }
    });
  }

}