import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/use_cases/auth/send_recover_password_email_use_case.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';

class RecoverPasswordController extends GetxController {

  final AlertsUtils _alertsUtils;
  final SendRecoverPasswordUseCase _sendRecoverPasswordUseCase;

  RecoverPasswordController({
    AlertsUtils? alertsUtils,
    SendRecoverPasswordUseCase? sendRecoverPasswordUseCase
  }): 
  _alertsUtils = alertsUtils ?? locator.get<AlertsUtils>(),
  _sendRecoverPasswordUseCase = sendRecoverPasswordUseCase ?? locator.get<SendRecoverPasswordUseCase>();

  Future<void> sendRecoverPasswordEmail(String email) async {
    EasyLoading.show();
    try {
      await _sendRecoverPasswordUseCase.invoke(email);
      EasyLoading.dismiss();
      Get.back();
      _alertsUtils.showGenericAlert(
        title: "Correo de recuperación enviado",
        message: "Se envió un correo de recuperación a $email"
      );
    } catch(err) {
      EasyLoading.dismiss();
      String message = "Ocurrió un error desconocido, por favor intente nuevamente";
      if(err is FirebaseAuthException) {
       switch(err.code) {
        case "user-not-found":
          message = "El correo electrónico ingresado no se encuentra registrado";
        break;
       }
      }
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message: message
      );
    }
  }
}