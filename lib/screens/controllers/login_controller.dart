import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/use_cases/auth/login_with_email_and_password_use_case.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';

class LoginController extends GetxController {

  final AlertsUtils _alertsUtils;
  final LoginWithEmailAndPasswordUseCase _loginWithEmailAndPasswordUseCase;

  LoginController({
    AlertsUtils? alertsUtils,
    LoginWithEmailAndPasswordUseCase? loginWithEmailAndPasswordUseCase
  }): 
  _alertsUtils = alertsUtils ?? locator.get<AlertsUtils>(),
  _loginWithEmailAndPasswordUseCase = loginWithEmailAndPasswordUseCase ?? locator.get<LoginWithEmailAndPasswordUseCase>();

  Future<void> login(String email, String password) async {
    EasyLoading.show();
    try {
      await _loginWithEmailAndPasswordUseCase.invoke(email, password);
      EasyLoading.dismiss();
      Get.offAllNamed("/todoList");
    } catch(err) {
      EasyLoading.dismiss();
      String message = "Ocurrió un error desconocido, por favor intente nuevamente";
      if(err is FirebaseAuthException) {
       switch(err.code) {
        case "wrong-password":
          message = "Contraseña incorrecta, por favor intente nuevamente";
        break;
        case "user-not-found":
          message = "El correo electrónico ingresado no se encuentra registrado";
        break;
       }
      }
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message:  message
      );
    }
  }

}