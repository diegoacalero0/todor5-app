import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/use_cases/auth/logout_use_case.dart';

class LogoutController extends GetxController {
  final LogoutUseCase _logoutUseCase;

  LogoutController({
    LogoutUseCase? logoutUseCase
  }): _logoutUseCase = logoutUseCase ?? locator.get<LogoutUseCase>();

  /// Method that logouts the current
  /// logged user
  Future<void> logout() async {
    await _logoutUseCase.invoke();
    Get.offAllNamed("/splash");
  }

} 