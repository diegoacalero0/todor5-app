import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/use_cases/auth/create_user_use_case.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';

class RegisterController extends GetxController {

  final AlertsUtils _alertsUtils;
  final CreateUserUseCase _createUserUseCase;

  RegisterController({
    AlertsUtils? alertsUtils,
    CreateUserUseCase? createUserUseCase
  }): _createUserUseCase = createUserUseCase ?? locator.get<CreateUserUseCase>(),
    _alertsUtils = alertsUtils ?? locator.get<AlertsUtils>();

  Future<void> createUser(String email, String password) async {
    EasyLoading.show();
    try {
      await _createUserUseCase.invoke(email, password);
      EasyLoading.dismiss();
      Get.offAllNamed("/todoList");
    } catch(err) {
      EasyLoading.dismiss();
      String message = "Ocurrió un error desconocido, por favor intente nuevamente";
      if(err is FirebaseAuthException) {
        switch(err.code) {
          case "email-already-in-use":
            message = "El correo electrónico ya se encuentra registrado";
          break;
          case "weak-password":
          message = "La contraseña debe tener al menos 6 caracteres";
          break;
        }
      }
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message: message
      );
    }
  }
}