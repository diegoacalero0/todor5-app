import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/route_manager.dart';
import 'package:get/state_manager.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/models/task_model.dart';
import 'package:todo_r5/use_cases/tasks/change_task_state_use_case.dart';
import 'package:todo_r5/use_cases/tasks/create_task_use_case.dart';
import 'package:todo_r5/use_cases/tasks/get_tasks_use_case.dart';
import 'package:todo_r5/use_cases/tasks/remove_task_use_case.dart';
import 'package:todo_r5/utilities/alerts_utils.dart';

class TodosController extends GetxController {
  
  final AlertsUtils _alertsUtils;
  final ChangeTaskStateUseCase _changeTaskStateUseCase;
  final CreateTaskUseCase _createTaskUseCase;
  final GetTasksUseCase _getTasksUseCase;
  final RemoveTaskUseCase _removeTaskUseCase;

  TodosController({
    AlertsUtils? alertsUtils,
    ChangeTaskStateUseCase? changeTaskStateUseCase,
    CreateTaskUseCase? createTaskUseCase,
    GetTasksUseCase? getTasksUseCase,
    RemoveTaskUseCase? removeTaskUseCase
  }): 
  _alertsUtils = alertsUtils ?? locator.get<AlertsUtils>(),
  _changeTaskStateUseCase = changeTaskStateUseCase ?? locator.get<ChangeTaskStateUseCase>(),
  _createTaskUseCase = createTaskUseCase ?? locator.get<CreateTaskUseCase>(),
  _getTasksUseCase = getTasksUseCase ?? locator.get<GetTasksUseCase>(),
  _removeTaskUseCase = removeTaskUseCase ?? locator.get<RemoveTaskUseCase>();

  RxList<TaskModel> tasks = <TaskModel>[].obs;

  Future<void> createTask(String title, String description) async {
    EasyLoading.show();
    try {
      await _createTaskUseCase.invoke(title, description);
      EasyLoading.dismiss();
      Get.back();
    } catch(err) {
      EasyLoading.dismiss();
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message: err.toString()
      );
    }
  }

  void getTasks() {
    _getTasksUseCase.invoke().stream.listen((List<TaskModel> newTasks) {
      tasks(newTasks);
      tasks.refresh();
    });
  }

  void setShowTranslate(bool value, int index) {
    tasks[index].showTranslate = value;
    tasks.refresh();
  }

  Future<void> removeTask(String taskId) async {
    EasyLoading.show();
    try {
      await _removeTaskUseCase.invoke(taskId);
      EasyLoading.dismiss();
      Get.showSnackbar(const GetSnackBar(
        title: "Tarea eliminada correctamente",
        message: "La tarea ha sido eliminada y no podrá ser recuperada",
        duration: Duration(seconds: 2),
      ));
    } catch(err) {
      EasyLoading.dismiss();
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message: err.toString()
      );
    }
  }

  Future<void> changeTaskState(String taskId, bool newValue) async {
    try {
      await _changeTaskStateUseCase.invoke(taskId, newValue);
      Get.showSnackbar(GetSnackBar(
        title: newValue ? "Tarea completada" : "Tarea NO completada",
        message: newValue ? "La tarea ha sido marcada como completada" : "La tarea ha sido marcada como NO completada",
        duration: const Duration(seconds: 2),
      ));
    } catch(err) {
      _alertsUtils.showGenericAlert(
        title: "Ocurrió un error",
        message: err.toString()
      );
    }
  }

}