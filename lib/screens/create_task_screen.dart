import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:todo_r5/screens/controllers/todo_controller.dart';
import 'package:todo_r5/widgets/custom_main_button.dart';
import 'package:todo_r5/widgets/custom_text_form_field.dart';

class CreateTaskScreen extends StatefulWidget {

    const CreateTaskScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _CreateTaskScreenState();

}

class _CreateTaskScreenState extends State<CreateTaskScreen> {

  final TodosController _controller = Get.put(TodosController());

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Crear tarea")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        child: Column(
          children: <Widget>[
            _createTextFieldTitle(),
            const SizedBox(height: 8),
            _createTextFieldDescription(),
            const SizedBox(height: 8),
            _createMainButton()
          ],
        ),
      ),
    );
  }

  Widget _createTextFieldTitle() {
    return CustomFilledTextFormField(
      controller: _titleController,
      hintText: "Título",
      textInputAction: TextInputAction.next,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  Widget _createTextFieldDescription() {
    return CustomFilledTextFormField(
      controller: _descriptionController,
      hintText: "Descripción",
      textInputAction: TextInputAction.done,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  Widget _createMainButton() {
    return CustomMainButton(
      text: "Crear tarea",
      onTap: () {
        _controller.createTask(_titleController.text, _descriptionController.text);
      }
    );
  }

}
