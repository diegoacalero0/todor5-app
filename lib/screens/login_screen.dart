import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:todo_r5/constants/assets_constants.dart';
import 'package:todo_r5/screens/controllers/login_controller.dart';
import 'package:todo_r5/widgets/custom_main_button.dart';
import 'package:todo_r5/widgets/custom_text_form_field.dart';
import 'package:get/get_instance/get_instance.dart';

class LoginScreen extends StatefulWidget {

    const LoginScreen({Key? key}) : super(key: key);

    @override
    State<StatefulWidget> createState() => _LoginScreenState();

}

class _LoginScreenState extends State<LoginScreen> {

  final LoginController _controller = Get.put(LoginController());

  final _formKey = GlobalKey<FormState>(); 
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Iniciar sesión")),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _createLottie(),
                _createTextFieldEmail(),
                const SizedBox(height: 8),
                _createTextFieldPassword(),
                const SizedBox(height: 8),
                _createMainButton(),
                const SizedBox(height: 8),
                _createRecoverPasswordButton(),
                _createOrRegister()
              ],
            ),
            key: _formKey,
          )
        ),
      )
    );
  }

  Widget _createLottie() {
    return SizedBox(
      width: 128,
      child: Lottie.asset(kLottieSplash)
    );
  }

  Widget _createTextFieldEmail() {
    return CustomFilledTextFormField(
      controller: _emailController,
      hintText: "Correo electrónico",
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);

        if(!emailValid) {
          return "Has ingresado un correo electrónico incorrecto. Inténtalo de nuevo.";
        }
      },
    );
  }

  Widget _createTextFieldPassword() {
    return CustomFilledTextFormField(
      controller: _passwordController,
      hintText: "Contraseña",
      obscureText: true,
      textInputAction: TextInputAction.done,
      validator: (String? text) {
        if(text == null || text.trim() == "") {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  Widget _createMainButton() {
    return CustomMainButton(
      onTap: () {
        if(_formKey.currentState?.validate() == true) {
          _controller.login(_emailController.text, _passwordController.text);
        }
      },
      text: "Iniciar sesión"
    );
  }

  Widget _createRecoverPasswordButton() {
    return TextButton(
      onPressed: () {
        Get.toNamed("/recoverPassword");
      },
      child: const Text("¿Olvidaste la contraseña?")
    );
  }

  Widget _createOrRegister() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: _createDividerLine()),
            const SizedBox(width: 8),
            Text(
              "Ó",
              style: Theme.of(context).textTheme.bodyText1?.copyWith(
                color: Theme.of(context).dividerColor
              )
            ),
            const SizedBox(width: 8),
            Expanded(child: _createDividerLine())
          ],
        ),
        const SizedBox(height: 16),
        CustomMainButton(
          onTap: _navigateToRegisterScreen,
          text: "Crear cuenta",
        )
      ],
    );
  }

  Widget _createDividerLine() {
    return Container(
      width: double.infinity,
      height: 0.5,
      color: Theme.of(context).dividerColor,
    );
  }

  void _navigateToRegisterScreen() {
    Get.toNamed("/register");
  }

}
