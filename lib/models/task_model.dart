import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todo_r5/constants/firebase_constants.dart';

class TaskModel {
  final String id;
  final String title;
  final String description;
  final bool isCompleted;
  final DateTime createdDate;
  final String? translatedTitle;
  final String? translatedDescription;
  bool showTranslate;

  TaskModel({
    required this.id,
    required this.title,
    required this.description,
    required this.isCompleted,
    required this.createdDate,
    required this.translatedTitle,
    required this.translatedDescription,
    this.showTranslate = false
  });

  factory TaskModel.fromFirebaseMap(String id, Map<String, dynamic> map) {
    return TaskModel(
      id: id,
      title: map[kFirestoreTitleField] ?? "",
      description: map[kFirestoreDescriptionField] ?? "",
      isCompleted: map[kFirestoreIsCompletedField] ?? false,
      createdDate: (map[kFirestoreCreatedAtField] as Timestamp).toDate(),
      translatedTitle: map[kFirestoreTranslatedTitleField],
      translatedDescription: map[kFirestoreTranslatedDescriptionField]
    );
  }

}