import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AlertsUtils {
  void showGenericAlert({ required String title, required String message, List<Widget>? actions }) {
    Get.defaultDialog(
      title: title,
      content: Text(message),
      actions: actions ?? [
        TextButton(onPressed: () {
          Get.back();
        }, child:  const Text("Aceptar"))
      ]
    );
  }
}