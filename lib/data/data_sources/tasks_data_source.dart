import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/constants/firebase_constants.dart';
import 'package:todo_r5/models/task_model.dart';

class TasksDataSource {

  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  TasksDataSource({
    FirebaseAuth? firebaseAuth,
    FirebaseFirestore? firestore
  }): 
  _firebaseAuth = firebaseAuth ?? locator.get<FirebaseAuth>(),
  _firestore = firestore ?? locator.get<FirebaseFirestore>();

  /// Method that creates a tasks associated
  /// to the logged user
  Future<void> createTask(String title, String description) async {
    try {
      final User? currentUser = _firebaseAuth.currentUser;
      if(currentUser != null) {
        String userEmail = currentUser.email ?? "";
        await _firestore.collection(kFirestoreUsersCollection).doc(userEmail).collection(kFirestoreTasksCollection).add({
          kFirestoreTitleField: title,
          kFirestoreDescriptionField: description,
          kFirestoreIsCompletedField: false,
          kFirestoreCreatedAtField: DateTime.now()
        });
      }
    } catch(err) {
      rethrow;
    }
  }

  /// Method that returns a stream with the lists of tasks
  StreamController<List<TaskModel>> getTasks() {
    final _controller = StreamController<List<TaskModel>>();

    final User? currentUser = _firebaseAuth.currentUser;

    List<TaskModel> tasks = [];
    List<TaskModel> completedTasks = [];

    if(currentUser != null) {
      String userEmail = currentUser.email ?? "";
      _firestore.collection(kFirestoreUsersCollection).doc(userEmail).collection(kFirestoreTasksCollection)
      .orderBy(kFirestoreCreatedAtField, descending: true)
      .snapshots().listen((QuerySnapshot<Map<String, dynamic>> event) {

        bool firstTime = tasks.isEmpty && completedTasks.isEmpty;

        tasks.clear();
        completedTasks.clear();

        for(QueryDocumentSnapshot<Map<String, dynamic>> doc in event.docs) {
          final TaskModel task = TaskModel.fromFirebaseMap(doc.id, doc.data());
          if(task.isCompleted && firstTime) {
            completedTasks.add(task);
          } else {
            tasks.add(task);
          }
        }
        tasks.addAll(completedTasks);
        _controller.add(tasks);
      });
    }

    return _controller;
  }

  /// Method that remove a task from the user tasks lists
  Future<void> removeTask(String taskId) async {
    try {
      final User? currentUser = _firebaseAuth.currentUser;
      if(currentUser != null) {
        String userEmail = currentUser.email ?? "";
        await _firestore.collection(kFirestoreUsersCollection).doc(userEmail).collection(kFirestoreTasksCollection).doc(taskId).delete();
      }
    } catch(err) {
      rethrow;
    }
  }

  Future<void> changeTaskState(String taskId, bool newState) async {
    try {
      final User? currentUser = _firebaseAuth.currentUser;
      if(currentUser != null) {
        String userEmail = currentUser.email ?? "";
        await _firestore.collection(kFirestoreUsersCollection).doc(userEmail).collection(kFirestoreTasksCollection).doc(taskId).set({
          kFirestoreIsCompletedField: newState
        }, SetOptions(merge: true));
      }
    } catch(err) {
      rethrow;
    }
  }

}