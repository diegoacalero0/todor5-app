import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';

class AuthDataSource {

  final FirebaseAuth _firebaseAuth;

  AuthDataSource({
    FirebaseAuth? firebaseAuth
  }): _firebaseAuth = firebaseAuth ?? locator.get<FirebaseAuth>();

  /// Method that returns the current logged user
  /// from FirebaseAuth
  /// otherwise returns null 
  User? getCurrentUser() {
    return _firebaseAuth.currentUser;
  } 

  /// Method that creates and new user in firebase
  /// with email and password and returns it user
  Future<User?> registerWithEmailAndPassword(String email, String password) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      return _firebaseAuth.currentUser;
    } catch(err) {
      rethrow;
    }
  }

  /// Method that logouts the user from firebase
  Future<void> logout() async {
    await _firebaseAuth.signOut();
  }

  /// Method that log the user into firebase authentication system
  /// using email and password
  Future<User?> loginWithEmailAndPassword(String email, String password) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      return _firebaseAuth.currentUser;
    } catch(err) {
      rethrow;
    }
  }

  /// Method that sent the recover password email
  /// from firebase 
  Future<void> sendRecoverPasswordEmail(String email) async {
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
    } catch(err) {
      rethrow;
    }
  }
}