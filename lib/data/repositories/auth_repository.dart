import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/data_sources/auth_data_souce.dart';

class AuthRepository {
  final AuthDataSource _authDataSource;

  AuthRepository({
    AuthDataSource? authDataSource
  }): _authDataSource = authDataSource ?? locator.get<AuthDataSource>();

  /// Method that returns the current logged user
  /// otherwise returns null 
  User? getCurrentUser() => _authDataSource.getCurrentUser();

  /// Method that creates a new account
  /// using the email and password
  Future<User?> registerWithEmailAndPassword(String email, String password) => _authDataSource.registerWithEmailAndPassword(email, password);

  /// Method that logouts the user
  Future<void> logout() => _authDataSource.logout();

  /// Method that log in the user using email and password
  Future<User?> loginWithEmailAndPassword(String email, String password) => _authDataSource.loginWithEmailAndPassword(email, password);

  /// Method that send a recover password email to the email
  /// provided
  Future<void> sendRecoverPasswordEmail(String email) => _authDataSource.sendRecoverPasswordEmail(email);
}