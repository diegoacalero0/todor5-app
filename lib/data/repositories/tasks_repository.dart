import 'dart:async';

import 'package:todo_r5/base/service_locator.dart';
import 'package:todo_r5/data/data_sources/tasks_data_source.dart';
import 'package:todo_r5/models/task_model.dart';

class TasksRepository {
  final TasksDataSource _tasksDataSource;

  TasksRepository({
    TasksDataSource? tasksDataSource
  }): _tasksDataSource = tasksDataSource ?? locator.get<TasksDataSource>();

  /// Method that creates a tasks
  Future<void> createTask(String title, String description) => _tasksDataSource.createTask(title, description);

  /// Method that returns a stream with the lists of tasks
  StreamController<List<TaskModel>> getTasks() => _tasksDataSource.getTasks();

  /// Method that remove a task from the user tasks lists
  Future<void> removeTask(String taskId) => _tasksDataSource.removeTask(taskId);

  /// Method that change the state of a task
  Future<void> changeTaskState(String taskId, bool newState) => _tasksDataSource.changeTaskState(taskId, newState);
}